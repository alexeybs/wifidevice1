#include <i18n.h>
#include <IRac.h>
#include <IRrecv.h>
#include <IRremoteESP8266.h>
#include <IRsend.h>
#include <IRtext.h>
#include <IRtimer.h>
#include <IRutils.h>
#include <ir_Amcor.h>
#include <ir_Argo.h>
#include <ir_Coolix.h>
#include <ir_Daikin.h>
#include <ir_Electra.h>
#include <ir_Fujitsu.h>
#include <ir_Goodweather.h>
#include <ir_Gree.h>
#include <ir_Haier.h>
#include <ir_Hitachi.h>
#include <ir_Kelvinator.h>
#include <ir_LG.h>
#include <ir_Magiquest.h>
#include <ir_Midea.h>
#include <ir_Mitsubishi.h>
#include <ir_MitsubishiHeavy.h>
#include <ir_NEC.h>
#include <ir_Neoclima.h>
#include <ir_Panasonic.h>
#include <ir_Samsung.h>
#include <ir_Sharp.h>
#include <ir_Tcl.h>
#include <ir_Teco.h>
#include <ir_Toshiba.h>
#include <ir_Trotec.h>
#include <ir_Vestel.h>
#include <ir_Whirlpool.h>

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>

#include <FastLED.h>

#define NUM_LEDS 36
#define DATA_PIN 5
#define CLOCK_PIN 13

// This is an array of leds.  One item for each led in your strip.
CRGB leds[NUM_LEDS];

byte ledMode = 0, ledCounter = 0, ledColorRed = 255, ledColorGreen = 255, ledColorBlue = 255;
boolean ledIsBlack = true,  ledCounterPlus = true;
byte red = 255, green = 0, blue = 0;
String ledModeNames [] = {
  "Off", "Color", "Pulse", "Pixel run", "Color grow", "Color run", "Rainbow", "Rainbow fade", "Rainbow run", "Cop", "RGB run"
  };

#define EVERY_MS(x) \
  static uint32_t tmr;\
  bool flag = millis() - tmr >= (x);\
  if (flag) tmr = millis();\
  if (flag)
//===========================

#ifndef STASSID
#define STASSID "ASUS1"
#define STAPSK  "a1e9cuyh871379"
#endif

const char* ssid = STASSID;
const char* password = STAPSK;

#define TOKEN  "hw2ALdxVpwxJzC6Tf5tP9Te7"

boolean lampIsOn = 0;

byte btnFlag = 0;      // флажок нажатия кнопки
byte btn;               // переменная, харнящая состояние кнопки
unsigned long lastPress;   // таймер для фильтра дребезга

// Create an instance of the server
// specify the port to listen on as an argument
ESP8266WebServer server(80);

#define PIN_LAMP D0
#define PIN_IR D3
#define PIN_BTN D7

IRrecv irrecv(PIN_IR);
decode_results results;

void setup() {
  Serial.begin(115200);

  irrecv.enableIRIn();
//  pinMode(PIN_IR, INPUT_PULLUP);

  // prepare LED
  pinMode(PIN_LAMP, OUTPUT);
  digitalWrite(PIN_LAMP, !lampIsOn);

  pinMode(PIN_BTN, INPUT);

  FastLED.addLeds<WS2811, DATA_PIN, BRG>(leds, NUM_LEDS);

  // Connect to WiFi network
  Serial.println();
  Serial.println();
  Serial.print(F("Connecting to "));
  Serial.println(ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(F("."));
  }
  Serial.println();
  Serial.println(F("WiFi connected"));

  // Print the IP address
  Serial.println(WiFi.localIP());

  // Назначаем функции на запросы
  server.on("/lamp/info", handleInfo);
  server.on("/lamp/on", handleOn);
  server.on("/lamp/off", handleOff);
  server.on("/lamp/toggle", handleToggle);
  server.on("/lamp/timer", handleTimer);
  server.on("/led/toggle", handleLed);
  server.on("/led/setcolor", handleLedColor);
//  server.onNotFound(handleNotFound);

  // Стартуем сервер
  server.begin();
  Serial.println(F("Server started"));
}

void loop() {
  server.handleClient();
  ledWork();

  if( irrecv.decode(&results) ) {
//    Serial.println((long int)results.value, HEX);
    if( results.value == 0xFFC23D && millis() - lastPress > 125) {
      lastPress = millis();
      toggleLamp();
    }
    irrecv.resume();
  }

  btn = digitalRead(PIN_BTN);  // считать текущее положение кнопки
  if (btn != btnFlag) {
    Serial.println(btn);
    btnFlag = btn;
    toggleLamp();
  }
  /*
  if (btn == 1 && btnFlag == 0 && millis() - lastPress > 250) {  // если кнопка НАЖАТА, до этого была была ОТПУЩЕНА
    btnFlag = 1;                    // запоминаем, что нажимали кнопку
    lastPress = millis();            // запоминаем время

    toggleLamp();
  }
  if (btn == 0 && btnFlag == 1) {  // если кнопка ОТПУЩЕНА, и до этого была НАЖАТА
    btnFlag = 0;                    // запоминаем, что отпустили кнопку
  }
  */
}
