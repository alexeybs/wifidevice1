void ledWork() {
  switch(ledMode){
    case 2: {
      ledFadeOneColor(); //моргание плавное
      break;
    }
    case 3: {
      ledRunningPixel(); //бегущий пиксель
      break;
    }
    case 4: {
      colorGrow(); //color line
      break;
    }
    case 5: {
      runningLights(); //бегущие огни
      break;
    }
    case 6: {
      ledRainbow(); //радуга плавная
      break;
    }
    case 7: {
      ledRainbowFade(); //радуга мигающая сплошная
      break;
    }
    case 8: {
      ledRainbowRunning(); //радуга бегущая
      break;
    }
    case 9: {
      ledPoliceStrobes(); //cops flasher
      break;
    }
    case 10: {
      rgbRun(); //rgb propeller
      break;
    }
    
  }
}

void ledFadeOneColor() {
  EVERY_MS(50) {
    byte c = 15;
    byte r = int(ledColorRed / c);
    byte g = int(ledColorGreen / c);
    byte b = int(ledColorBlue / c);
    if(ledIsBlack) ledCounter++;
    else ledCounter--;
    for (int i = 0; i < NUM_LEDS; i++ ) {
      leds[i] = CRGB(ledColorRed, ledColorGreen, ledColorBlue);
//            leds[i].subtractFromRGB(100-ledCounter);
//            leds[i] -= 100 - ledCounter;
//            leds[i].addToRGB(ledCounter);
      leds[i] -= CRGB(ledCounter * r, ledCounter * g, ledCounter * b);
    }
    FastLED.show();
    if(ledCounter >= c) {
      ledIsBlack = false;
      
    }
    if(ledCounter <= 0) {
      ledIsBlack = true;
    }
  }
}

void ledRunningPixel() {
  EVERY_MS(50) {
    for (int i = 0; i < NUM_LEDS; i++ ) {
      if(i == ledCounter) leds[i] = CRGB(ledColorRed, ledColorGreen, ledColorBlue);
      else leds[i] = CRGB::Black;
    }
    if(ledCounter == NUM_LEDS) ledCounterPlus = false;
    if(ledCounter == 0) ledCounterPlus = true;
    if(ledCounterPlus) ledCounter++;
    else ledCounter--;
    FastLED.show();
  }
}

void ledRainbow() {
  EVERY_MS(20) {
    for (int i = 0; i < NUM_LEDS; i++ ) {
      leds[i] = CHSV(ledCounter + i * 3, 255, 255);  // HSV. Увеличивать HUE (цвет)
      // умножение i уменьшает шаг радуги
    }
    ledCounter++;        // ledCounter меняется от 0 до 255 (тип данных byte)
    FastLED.show();
  }
}

void ledRainbowFade() {
  EVERY_MS(30) {
    byte s = 5;
    if(ledCounter == 0) {
      if(green < 255) green += s;
      else ledCounter = 1;
    }
    else if(ledCounter == 1) {
      if(red > 0) red -= s;
      else ledCounter = 2;
    }
    else if(ledCounter == 2) {
      if(blue < 255) blue += s;
      else ledCounter = 3;
    }
    else if(ledCounter == 3) {
      if(green > 0) green -= s;
      else ledCounter = 4;
    }
    else if(ledCounter == 4) {
      if(red < 255) red += s;
      else ledCounter = 5;
    }
    else if(ledCounter == 5) {
      if(blue > 0) blue -= s;
      else ledCounter = 0;
    }
    for (int i = 0; i < NUM_LEDS; i++ ) {
      leds[i] = CRGB(red, green, blue);
    }
    FastLED.show();
  }
}

void ledRainbowRunning() {
  EVERY_MS(100) {
    for (int i = 0; i < NUM_LEDS; i++ ) {
      if((i - ledCounter) % 7 == 0) leds[i] = CRGB::Red;
      if((i - 1 - ledCounter) % 7 == 0) leds[i] = CRGB(255, 127, 0);
      if((i - 2 - ledCounter) % 7 == 0) leds[i] = CRGB::Yellow;
      if((i - 3 - ledCounter) % 7 == 0) leds[i] = CRGB::Green;
      if((i - 4 - ledCounter) % 7 == 0) leds[i] = CRGB::Aqua;
      if((i - 5 - ledCounter) % 7 == 0) leds[i] = CRGB::Blue;
      if((i - 6 - ledCounter) % 7 == 0) leds[i] = CRGB::Purple;
    }
    ledCounter++;
    if(ledCounter >= 7) ledCounter = 0;
    FastLED.show();
  }
}

void ledPoliceStrobes() {
  int TOP_INDEX = int(NUM_LEDS / 2);
  EVERY_MS(25) {
    if(ledCounter % 2 != 0) fillColorAll(0, 0, 0);
    else {
      if(ledCounter < 10) {
        for (int i = 0 ; i < TOP_INDEX; i++ ) {
            leds[i] = CRGB::Blue;
        }
      }
      else {
        for (int i = TOP_INDEX ; i < NUM_LEDS; i++ ) {
            leds[i] = CRGB::Red;
        }
      }
      FastLED.show();
    }
    ledCounter++;
    if(ledCounter > 19) ledCounter = 0;
 }
}

void rgbRun() {
  EVERY_MS(25) {
    ledCounter++;
    int N3  = int(NUM_LEDS / 3);
    int N6  = int(NUM_LEDS / 6);
    int N12 = int(NUM_LEDS / 12);
    for (int i = 0; i < N3; i++ ) {
      int j0 = (ledCounter + i + NUM_LEDS - N12) % NUM_LEDS;
      int j1 = (j0 + N3) % NUM_LEDS;
      int j2 = (j1 + N3) % NUM_LEDS;
      leds[j0] = CRGB::Red;
      leds[j1] = CRGB::Green;
      leds[j2] = CRGB::Blue;
    }
    FastLED.show();
  }
}

void colorGrow() {
  EVERY_MS(50) {
    if(ledCounterPlus) leds[ledCounter] = CRGB(ledColorRed, ledColorGreen, ledColorBlue);
    else leds[ledCounter] = CRGB(0, 0, 0);
    FastLED.show();
    ledCounter++;
    if(ledCounter >= NUM_LEDS) {
      ledCounter = 0;
      ledCounterPlus = !ledCounterPlus;
    }
  }
}

void runningLights() {
  EVERY_MS(100) {
    byte b = 3;
    for (int i = 0; i < NUM_LEDS; i++ ) {
      if((i - ledCounter) % b == 0) leds[i] = CRGB::Black;
      else leds[i] = CRGB(ledColorRed, ledColorGreen, ledColorBlue);
    }
    ledCounter++;
    if(ledCounter >= NUM_LEDS) {
      ledCounter = 0;
      ledCounterPlus = !ledCounterPlus;
    }
    FastLED.show();
  }
}
