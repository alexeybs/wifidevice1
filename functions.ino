
// Да будет свет
void turnOnLamp(){
  lampIsOn = 1;
  digitalWrite(PIN_LAMP, !lampIsOn);
}

// Да будет тьма
void turnOffLamp(){
  lampIsOn = 0;
  digitalWrite(PIN_LAMP, !lampIsOn);
}

// Изменяем состояние лампы
void toggleLamp(){
  lampIsOn = !lampIsOn;
  digitalWrite(PIN_LAMP, !lampIsOn);
}

// Получаем от сервера команду включить
void handleInfo(){
  if(!checkToken()) {
    sendResponseTokenError();
    return;
  }
  Serial.println(F("info request"));
  sendResponse();
}

// Получаем от сервера команду включить
void handleOn(){
  if(!checkToken()) {
    sendResponseTokenError();
    return;
  }
  Serial.println(F("on request"));
  turnOnLamp();
  sendResponse();
}

// Получаем от сервера команду выключить
void handleOff(){
  if(!checkToken()) {
    sendResponseTokenError();
    return;
  }
  Serial.println(F("off request"));
  turnOffLamp();
  sendResponse();
}

// Получаем от сервера команду выключить
void handleToggle(){
  if(!checkToken()) {
    sendResponseTokenError();
    return;
  }
  Serial.println(F("toggle request"));
  toggleLamp();
  sendResponse();
}

// Таймер
void handleTimer(){
  if(!checkToken()) {
    sendResponseTokenError();
    return;
  }
  Serial.println(F("timer request"));
  String response = String("{lampIsOn: ") + String(lampIsOn) + String(", ip: '") + server.client().remoteIP().toString() + "'";
  for (int i = 0; i < server.args(); i++) {
    response += ", ";
    response += String(server.argName(i)) + String(": ");
    response += server.arg(i);
  }
  response += "}";
  server.send(200, "application/json", response);
}

// Светодиод
void handleLed(){
  if(!checkToken()) {
    sendResponseTokenError();
    return;
  }
  Serial.println(F("led"));
  toggleLed();
  sendResponse();
}
// Светодиод
void handleLedColor(){
  if(!checkToken()) {
    sendResponseTokenError();
    return;
  }
  Serial.println(F("led set color"));
  ledColorRed = (server.arg("red")).toInt();
  ledColorGreen = (server.arg("green")).toInt();
  ledColorBlue = (server.arg("blue")).toInt();
  if(ledMode == 1) fillColorLed();
  sendResponse();
}

void toggleLed(){
  ledMode++;
  if(ledMode > 10) ledMode = 0;
  switch(ledMode){
    case 1: {
      fillColorLed();
      break;
    }
    default: {
      fillBlackLed();
    }
  }
}

void fillColorLed(){
  fillColorAll(ledColorRed, ledColorGreen, ledColorBlue);
}

void fillColorAll(byte r, byte g, byte b){
  for (int i = 0; i < NUM_LEDS; i++ ) {
    leds[i] = CRGB(r, g, b);
  }
  FastLED.show();
}

void fillBlackLed(){
  FastLED.clear(true);
  ledCounter = 0;
  ledCounterPlus = true;
  ledIsBlack = true;
  red = 255;
  green = 0;
  blue = 0;
}

boolean checkToken() {
  return server.arg("token") == TOKEN;
}

void sendResponseTokenError() {
  String response = String("{error: 'invalid token'}");
  server.send(200, "application/json", response);
}

void sendResponse() {
//  String response = "HTTP/1.1 200 OK\r\nContent-Type: application/json\r\n\r\n";
  String response = String("{lampIsOn: ") + String(lampIsOn) + String(", ip: '") + server.client().remoteIP().toString();
  response += String("', ledMode: ") + String(ledMode);
  response += String(", ledModeName: '") + ledModeNames[ledMode];
  response += String("', ledColorRed: ") + String(ledColorRed);
  response += String(", ledColorGreen: ") + String(ledColorGreen);
  response += String(", ledColorBlue: ") + String(ledColorBlue) + "}";
  server.send(200, "application/json", response);
}
